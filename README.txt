How to integrate RoundCube Webmail with Drupal
==============================================

1)  Install RoundCube Webmail following the instructions on:

      http://trac.roundcube.net/wiki/Howto_Install

    Make sure to set a default IMAP host in RoundCube's config.

2)  At line 35 in the file index.php of RoundCube Webmail 0.2-beta,
    add a require_once statement that includes 'roundcube.inc'
    provided with this module. The result would look something like
    this:

      // init application and start session with requested task
      $RCMAIL = rcmail::get_instance();

      #################### BEGINNING OF DRUPAL INTEGRATION ####################
      require_once('/path/to/drupal/sites/all/modules/roundcube/roundcube.inc');
      ####################### END OF DRUPAL INTEGRATION #######################

      // init output class

3)  Optionally install Thickbox.

4)  Configure the settings on admin/settings/roundcube (Administer ›
    Site configuration › RoundCube).
