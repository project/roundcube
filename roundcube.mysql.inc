<?php



/**
 * @file
 * MySQL queries for RoundCube – a Drupal module that integrates RoundCube
 * Webmail 0.2 with Drupal.
 *
 * Each function below must return a string that can be passed into db_query()
 * as the query argument. The additional arguments that will be passed into
 * db_query() are passed in an array to the function. The function may use
 * these arguments to build the query, or alter them.
 */


/******************************************************************************
 * MYSQL QUERIES
 ******************************************************************************/

function _roundcube_db_get_password_query($args) {
  return <<<SQL
SELECT AES_DECRYPT(password, '%s') AS password FROM {roundcube_passwords} WHERE uid = %d
SQL;
}

function _roundcube_db_insert_password_query($args) {
  return <<<SQL
INSERT INTO {roundcube_passwords} (uid, password) VALUES (%d, AES_ENCRYPT('%s','%s'))
SQL;
}

function _roundcube_db_update_password_query($args) {
  return <<<SQL
UPDATE {roundcube_passwords} SET password = AES_ENCRYPT('%s','%s') WHERE uid = %d
SQL;
}

function _roundcube_db_delete_password_query($args) {
  return <<<SQL
DELETE FROM {roundcube_passwords} WHERE uid = %d
SQL;
}

function _roundcube_db_insert_credential_query() {
  return <<<SQL
INSERT INTO {roundcube_credentials} (id, uid, expires) VALUES ('%s', %d, %d)
SQL;
}

function _roundcube_db_get_credential_query() {
  return <<<SQL
SELECT uid, expires FROM {roundcube_credentials} WHERE id = '%s'
SQL;
}

function _roundcube_db_delete_credential_query() {
  return <<<SQL
DELETE FROM {roundcube_credentials} WHERE id = '%s'
SQL;
}
