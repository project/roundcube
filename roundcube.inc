<?php


 
/**
 * @file
 * At line 35 in the file index.php of RoundCube Webmail 0.2-beta,
 * add a require_once statement that includes this file.
 */


// If there is a 'drupal' query variable, it is a request from Drupal's
// RoundCube module to log in the user specified in the '_user' query string.
if ($drupal = $_GET['drupal']) {

  $handle = fopen($drupal, 'r');

  if ($handle !== false) {

    // If not logging out, the 'drupal' variable contains a one-time URL from
    // which we can get the user's password. Everytime a URL is passed on to us,
    // we must get the password, to allow the Drupal module to free it.
    $credential = fgets($handle);
    fclose($handle);

    // If there is not a session with a 'user_id' variable, the user is not
    // logged in.
    if (!$_SESSION['user_id']) {
      $RCMAIL->action = 'login';
      $_SESSION['temp'] = true;
      $_POST['_user'] = $_GET['_user'];
      $_POST['_pass'] = unserialize($credential);
      $_POST['_framed'] = 1;
    } 

  }
}
